const express = require('express')

const app = express()


app.get('/hello', (req,res,next) => {
    return res.status(200).json({
        hello: 'world'
    })
})

app.listen(3000, () => {
    console.log('The server is running in port 3000')
})